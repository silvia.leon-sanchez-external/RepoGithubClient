package com.example.github_client;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.example.github_client.dto.RepositoriesSearched;
import com.example.github_client.dto.Repository;

import java.io.IOException;
import java.util.Iterator;

import static com.example.github_client.MainActivity.gitHubAdapter;

public class RepositoriesActivity extends AppCompatActivity implements
        RepositoriesFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.content_repositories_list_framelayout, RepositoriesFragment.newInstance())
                .commit();
    }

    @Override
    public RepositoriesSearched onFragmentInteraction() {
        RepositoriesSearched repos;
        try {
            String name = getIntent().getStringExtra("name");
            String lang = getIntent().getStringExtra("lang");
            if (lang.isEmpty()) {
                repos = gitHubAdapter.searchRepositories(name);
            } else {
                repos = gitHubAdapter.searchRepositories(name);
                Iterator<Repository> iterator = repos.getItems().iterator();
                while(iterator.hasNext()){
                    Repository repo = iterator.next();
                    if (!repo.getLanguage().equalsIgnoreCase(lang)){
                        iterator.remove();
                    }
                }
            }

            if (!repos.getItems().isEmpty() && repos != null) {
                Log.d("Search", repos.getItems().get(0).toString());
            } else {
                Log.d("Search", "La busqueda no ha funcionado");
            }

            return repos;

        } catch (IOException e) {
            Log.e("Github_client", "Error al realizar la busqueda de repositorios", e);
            return null;
        }
    }

}
