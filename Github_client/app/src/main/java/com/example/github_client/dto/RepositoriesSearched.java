package com.example.github_client.dto;

import java.util.ArrayList;
import java.util.List;

public class RepositoriesSearched {

    private List<Repository> items;

    public RepositoriesSearched() {
        this.items = new ArrayList<>();
    }

    public RepositoriesSearched(List<Repository> items) {
        this.items = items;
    }

    public List<Repository> getItems() {
        return items;
    }

    public void setItems(List<Repository> items) {
        this.items = items;
    }
}
