package com.example.github_client;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.github_client.dto.Contributor;
import com.example.github_client.dto.RepositoriesSearched;
import com.example.github_client.dto.Repository;

import java.util.List;


public class RepositoriesFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private RepositoriesSearched repositories;
    private RecyclerView recyclerViewRepositories;

    public RepositoriesFragment() {
        // Required empty public constructor
    }

    public static RepositoriesFragment newInstance() {
        return new RepositoriesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_repositories, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof RepositoriesFragment.OnFragmentInteractionListener) {
            this.recyclerViewRepositories = getActivity().findViewById(R.id.recyclerViewRepositories);
            mListener = (RepositoriesFragment.OnFragmentInteractionListener) getActivity();
            repositories = mListener.onFragmentInteraction();
            RepositoryAdapter adapter = new RepositoryAdapter(repositories);
            adapter.setListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i = recyclerViewRepositories.getChildAdapterPosition(v);
                    Repository repositoryClicked = repositories.getItems().get(i);
                    Intent intent = new Intent(getActivity(), RepositoryActivity.class);
                    intent.putExtra("repo", repositoryClicked.getName());
                    intent.putExtra("owner", repositoryClicked.getOwner());
                    startActivity(intent);
                }
            });

            recyclerViewRepositories.setAdapter(adapter);
            recyclerViewRepositories.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL));
            recyclerViewRepositories.setLayoutManager(new LinearLayoutManager(getActivity()));

        } else {
            throw new RuntimeException(getActivity().toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        RepositoriesSearched onFragmentInteraction();
    }
}
