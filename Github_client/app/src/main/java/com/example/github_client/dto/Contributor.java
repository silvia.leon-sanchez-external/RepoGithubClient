package com.example.github_client.dto;

import com.google.gson.annotations.SerializedName;

public class Contributor extends User{


    @SerializedName("contributions")
    private final int contributions;

    public Contributor(String login, String avatar_url, int contributions) {
        super(login, avatar_url);
        this.contributions = contributions;
    }

    @Override
    public String toString() {
        return "Contributor{" +
                "contributions=" + contributions +
                ", login='" + super.getName() + '\'' +
                ", avatar_url='" + super.getAvatarUrl() + '\'' +
                '}';
    }

    public int getContributions() {
        return contributions;
    }
}
