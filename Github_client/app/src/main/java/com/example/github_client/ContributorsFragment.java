package com.example.github_client;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.github_client.dto.Contributor;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContributorsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContributorsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContributorsFragment extends Fragment {


    private List<Contributor> contributors;
    private RecyclerView recyclerViewContributors;

    private OnFragmentInteractionListener mListener;

    public ContributorsFragment() {
        // Required empty public constructor
    }

    public static ContributorsFragment newInstance() {
        return new ContributorsFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contributors, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof ContributorsFragment.OnFragmentInteractionListener) {
            this.recyclerViewContributors = getActivity().findViewById(R.id.recyclerViewContributors);
            mListener = (ContributorsFragment.OnFragmentInteractionListener) getActivity();
            contributors = mListener.onFragmentInteraction();
            ContributorAdapter adapter = new ContributorAdapter(contributors);
            adapter.setListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i = recyclerViewContributors.getChildAdapterPosition(v);
                    Contributor contributorClicked = contributors.get(i);
                    Intent intent = new Intent(getActivity(), ContributorDetailActivity.class);
                    intent.putExtra("user", contributorClicked.getName());
                    startActivity(intent);
                }
            });

            recyclerViewContributors.setAdapter(adapter);
            recyclerViewContributors.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL));
            recyclerViewContributors.setLayoutManager(new LinearLayoutManager(getActivity()));

        } else {
            throw new RuntimeException(getActivity().toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        List<Contributor> onFragmentInteraction();

    }
}
