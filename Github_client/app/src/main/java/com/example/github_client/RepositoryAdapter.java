package com.example.github_client;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.github_client.dto.RepositoriesSearched;
import com.example.github_client.dto.Repository;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolderRepositories> {

    private RepositoriesSearched repositories;
    private View.OnClickListener listener;

    public RepositoryAdapter(RepositoriesSearched repositories) {
        this.repositories = repositories;
    }

    @NonNull
    @Override
    public ViewHolderRepositories onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vistaVacia = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repository,parent,false);
        vistaVacia.setOnClickListener(listener);
        return new ViewHolderRepositories(vistaVacia);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderRepositories viewHolderRepositories, int i) {
        viewHolderRepositories.bindContributor(this.repositories.getItems().get(i));
    }

    @Override
    public int getItemCount() {
        return this.repositories.getItems().size();
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public static class ViewHolderRepositories extends RecyclerView.ViewHolder{

        private TextView textViewNameRepo;
        private TextView textViewLangRepo;
        private TextView textViewOwnerRepo;

        public ViewHolderRepositories(@NonNull View itemView) {
            super(itemView);
            this.textViewNameRepo = itemView.findViewById(R.id.textViewNameRepo);
            this.textViewLangRepo = itemView.findViewById(R.id.textViewLangRepo);
            this.textViewOwnerRepo = itemView.findViewById(R.id.textViewOwnerRepo);
        }

        // Cargar la informacion en cada item
        public void bindContributor(Repository r){
            this.textViewNameRepo.setText(r.getName());
            this.textViewLangRepo.setText(r.getLanguage());
            this.textViewOwnerRepo.setText(r.getOwner());
        }
    }

}
