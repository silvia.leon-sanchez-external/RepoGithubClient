package com.example.github_client.logic;

import com.example.github_client.dto.Contributor;
import com.example.github_client.dto.RepositoriesSearched;
import com.example.github_client.dto.Repository;
import com.example.github_client.dto.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GitHubAdapter {

    public static final String URL = "https://api.github.com";

    private GitHubInterface github;


    public GitHubAdapter() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .registerTypeAdapter(RepositoriesSearched.class, new RepositoriesSearchedDeserializer())
                .registerTypeAdapter(Repository.class, new RepositoryDeserializer())
                .serializeNulls()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        this.github = retrofit.create(GitHubInterface.class);
    }

    public List<Contributor> getContributors(String owner, String repo) throws IOException {
        Call<List<Contributor>> call = github.contributors(owner, repo);
        return call.execute().body();
    }

    public Repository getRepository(String owner, String repo) throws IOException {
        Call<Repository> call = github.repository(owner, repo);
        return call.execute().body();
    }

    public User getUser(String user) throws IOException {
        Call<User> call = github.user(user);
        return call.execute().body();
    }

    public RepositoriesSearched searchRepositories(String name) throws IOException {
        Call<RepositoriesSearched> call = github.searchRepositories(name);
        return call.execute().body();
    }


}
