package com.example.github_client;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.github_client.dto.Contributor;
import com.example.github_client.dto.Repository;
import com.example.github_client.dto.User;
import com.example.github_client.logic.GitHubAdapter;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import static com.example.github_client.MainActivity.gitHubAdapter;

public class ContributorDetailActivity extends AppCompatActivity implements
 ContributorDetailFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributor_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.content_contributor_detail_framelayout, ContributorDetailFragment.newInstance())
                .commit();
    }

    @Override
    public User onFragmentInteraction() {
        try {
            String user = getIntent().getStringExtra("user");
            return gitHubAdapter.getUser(user);
        } catch (IOException e) {
            Log.e("Github_client", "Error al realizar la peticion al servidor de GitHubInterface", e);
            return null;
        }
    }
}
