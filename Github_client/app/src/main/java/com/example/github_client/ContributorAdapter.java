package com.example.github_client;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.github_client.dto.Contributor;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ContributorAdapter extends RecyclerView.Adapter<ContributorAdapter.ViewHolderContributors> {

    private List<Contributor> contributorsList;
    private View.OnClickListener listener;

    public ContributorAdapter(List<Contributor> contributorsList) {
        this.contributorsList = contributorsList;
    }

    @NonNull
    @Override
    public ViewHolderContributors onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vistaVacia = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contributor,parent,false);
        vistaVacia.setOnClickListener(listener);
        return new ViewHolderContributors(vistaVacia);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderContributors viewHolderContributors, int i) {
        viewHolderContributors.bindContributor(this.contributorsList.get(i));
    }

    @Override
    public int getItemCount() {
        return this.contributorsList.size();
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public static class ViewHolderContributors extends RecyclerView.ViewHolder{

        // Mismos elementos que hay en el diseño del item
        private ImageView imageViewAvatarContributor;
        private TextView textViewLogin;
        private TextView textViewContributions;

        public ViewHolderContributors(@NonNull View itemView) {
            super(itemView);
            this.imageViewAvatarContributor = itemView.findViewById(R.id.imageViewAvatarContributor);
            this.textViewLogin = itemView.findViewById(R.id.textViewLogin);
            this.textViewContributions = itemView.findViewById(R.id.textViewContributions);
        }

        // Cargar la informacion en cada item
        public void bindContributor(Contributor c){
            Picasso.get()
                    .load(c.getAvatarUrl())
                    .fit()
                    .into(imageViewAvatarContributor);
            this.textViewLogin.setText(c.getName());
            this.textViewContributions.setText(c.getContributions()+"");
        }
    }

}
