package com.example.github_client;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.github_client.dto.User;
import com.squareup.picasso.Picasso;


public class ContributorDetailFragment extends Fragment {

    private ImageView imageViewAvatarColaboratorDetail;
    private TextView textViewLogin;
    private OnFragmentInteractionListener mListener;

    public ContributorDetailFragment() {
        // Required empty public constructor
    }

    public static ContributorDetailFragment newInstance() {
        return new ContributorDetailFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contributor_detail, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof ContributorDetailFragment.OnFragmentInteractionListener) {

            mListener = (ContributorDetailFragment.OnFragmentInteractionListener) getActivity();
            User user = mListener.onFragmentInteraction();

            this.textViewLogin = getActivity().findViewById(R.id.textViewLogin);
            this.textViewLogin.setText(user.getName());

            this.imageViewAvatarColaboratorDetail = getActivity().findViewById(R.id.imageViewAvatarColaboratorDetail);
            Log.d("picasso", user.getAvatarUrl());

            Picasso picasso = Picasso.get();
            picasso.setLoggingEnabled(true);
            picasso.load(user.getAvatarUrl())
                    .fit()
                    .into(imageViewAvatarColaboratorDetail);
        } else {
            throw new RuntimeException(getActivity().toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        User onFragmentInteraction();
    }
}
