package com.example.github_client;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.example.github_client.dto.Contributor;

import java.io.IOException;
import java.util.List;

import static com.example.github_client.MainActivity.gitHubAdapter;

public class ContributorsActivity extends AppCompatActivity implements
        ContributorsFragment.OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributors);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.content_contributors_list_framelayout, ContributorsFragment.newInstance())
                .commit();
    }


    @Override
    public List<Contributor> onFragmentInteraction() {
        try {

            return gitHubAdapter.getContributors(getIntent().getStringExtra("owner"), getIntent().getStringExtra("repo"));
        } catch (IOException e) {
            Log.e("Github_client", "Error al realizar la peticion de contribuidores al servidor de GitHub", e);
            return null;
        }
    }


}
