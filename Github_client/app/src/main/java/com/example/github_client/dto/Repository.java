package com.example.github_client.dto;

import com.google.gson.annotations.SerializedName;

public class Repository {

    @SerializedName("name")
    private final String name;

    @SerializedName("description")
    private final String description;

    @SerializedName("language")
    private final String language;

    private final String owner;

    public Repository(String name, String description, String language, String owner) {
        this.name = name;
        this.description = description;
        this.language = language;
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Repository{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", language='" + language + '\'' +
                ", owner='" + owner + '\'' +
                '}';
    }

    public String getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLanguage() {
        return language;
    }
}
