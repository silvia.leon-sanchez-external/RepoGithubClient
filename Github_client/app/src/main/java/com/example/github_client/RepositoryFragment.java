package com.example.github_client;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.github_client.dto.Repository;


public class RepositoryFragment extends Fragment {

    private TextView textViewRepoName;
    private TextView textViewRepoLang;
    private TextView textViewRepoOwner;
    private TextView textViewRepoDescription;
    private Button buttonContributors;

    private OnFragmentInteractionListener mListener;

    public RepositoryFragment() {
        // Required empty public constructor
    }

    public static RepositoryFragment newInstance() {
        RepositoryFragment fragment = new RepositoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_repository, container, false);

        Context context = view.getContext();

        this.textViewRepoName = view.findViewById(R.id.textViewRepoName);
        this.textViewRepoLang = view.findViewById(R.id.textViewRepoLang);
        this.textViewRepoOwner = view.findViewById(R.id.textViewRepoOwner);
        this.textViewRepoDescription = view.findViewById(R.id.textViewRepoDescription);
        this.buttonContributors = view.findViewById(R.id.buttonContributors);
        this.buttonContributors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClickContributors();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) getActivity();

            Repository repository = mListener.onFragmentInteraction();
            this.textViewRepoName.setText(repository.getName());
            this.textViewRepoDescription.setText(repository.getDescription());
            this.textViewRepoLang.setText(repository.getLanguage());
            this.textViewRepoOwner.setText(repository.getOwner());


        } else {
            throw new RuntimeException(getActivity().toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        Repository onFragmentInteraction();

        void onClickContributors();
    }
}
