package com.example.github_client.logic;

import com.example.github_client.dto.Contributor;
import com.example.github_client.dto.RepositoriesSearched;
import com.example.github_client.dto.Repository;
import com.example.github_client.dto.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GitHubInterface {

    @GET("/users/{user}")
    Call<User> user(@Path("user") String user);

    @GET("/repos/{owner}/{repo}")
    Call<Repository> repository(@Path("owner") String owner, @Path("repo") String repo);

    @GET("/repos/{owner}/{repo}/contributors")
    Call<List<Contributor>> contributors(@Path("owner") String owner, @Path("repo") String repo);

    @GET("/search/repositories")
    Call<RepositoriesSearched> searchRepositories(@Query ("q") String name);

}
