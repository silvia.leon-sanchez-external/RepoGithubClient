package com.example.github_client.logic;

import android.util.Log;

import com.example.github_client.dto.RepositoriesSearched;
import com.example.github_client.dto.Repository;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class RepositoryDeserializer implements JsonDeserializer<Repository> {
    @Override
    public Repository deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        String name = "", description = "", language = "", owner = "";
        JsonObject jsonRepository = json.getAsJsonObject();

        try{
            name = jsonRepository.getAsJsonPrimitive("name").getAsString();
        }catch(Exception ex){
            Log.d("Json",ex.getMessage());
        }

        try{
            description = jsonRepository.getAsJsonPrimitive("description").getAsString();
        }catch(Exception ex){
            Log.d("Json",ex.getMessage());
        }

        try{
            language = jsonRepository.getAsJsonPrimitive("language").getAsString();
        }catch(Exception ex){
            Log.d("Json",ex.getMessage());
        }

        JsonObject jsonOwner = jsonRepository.getAsJsonObject("owner");
        try{
            owner = jsonOwner.getAsJsonPrimitive("login").getAsString();
        }catch(Exception ex){
            Log.d("Json",ex.getMessage());
        }

        return new Repository(name,description,language,owner);
    }
}
