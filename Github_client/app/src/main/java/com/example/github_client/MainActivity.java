package com.example.github_client;

import android.content.Intent;
import android.os.Build;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.github_client.dto.RepositoriesSearched;
import com.example.github_client.dto.Repository;
import com.example.github_client.logic.GitHubAdapter;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    /*
    Glide.with(this)
    .load(URL)
    .error(R.drawable.error)
    .placeholder(R.drawable.placeholder)
    .into(image);
     */
    private ImageView imageViewGithubIcon;
    private EditText editTextSearchName;
    private EditText editTextSearchLang;
    private Button buttonSearch;

    public static final GitHubAdapter gitHubAdapter = new GitHubAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.imageViewGithubIcon = findViewById(R.id.imageViewGithubIcon);
        this.editTextSearchName = findViewById(R.id.editTextSearchName);
        this.editTextSearchLang = findViewById(R.id.editTextSearchLang);
        this.buttonSearch = findViewById(R.id.buttonSearch);
        this.buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickButtonSearch();
            }
        });

        int SDK_INT = Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    public void onClickButtonSearch() {

        try {
            String name = editTextSearchName.getText().toString();
            String lang = editTextSearchLang.getText().toString();
            if (name.isEmpty()) {
                Toast.makeText(this, "Tienes que escribir un nombre", Toast.LENGTH_LONG).show();
            } else {
                Intent i = new Intent(MainActivity.this, RepositoriesActivity.class);
                i.putExtra("name", name);
                i.putExtra("lang", lang);
                startActivity(i);
            }

        } catch (Exception e) {
            Log.e("Search", e.getMessage());
        }
    }

}
