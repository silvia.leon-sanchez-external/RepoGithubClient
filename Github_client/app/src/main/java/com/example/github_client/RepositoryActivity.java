package com.example.github_client;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.example.github_client.dto.Repository;

import java.io.IOException;

import static com.example.github_client.MainActivity.gitHubAdapter;

public class RepositoryActivity extends AppCompatActivity implements
        RepositoryFragment.OnFragmentInteractionListener {

    private String repository;
    private String owner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        owner = getIntent().getStringExtra("owner");
        repository = getIntent().getStringExtra("repo");
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.content_repo_framelayout, RepositoryFragment.newInstance())
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!internetConnected()) {
            Log.e("Github_client", "No hay conexion a internet");
        }
    }

    public boolean internetConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public Repository onFragmentInteraction() {

        try {
            return gitHubAdapter.getRepository(owner, repository);
        } catch (IOException e) {
            Log.e("Github_client", "Error al realizar la peticion al servidor de GitHubInterface", e);
            return null;
        }

    }

    @Override
    public void onClickContributors() {
        Intent i = new Intent(this, ContributorsActivity.class);
        i.putExtra("repo", repository);
        i.putExtra("owner", owner);
        startActivity(i);
    }
}
